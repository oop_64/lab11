package com.thanakit;

public class Bird extends Animal implements Flyable,Walkable{

    public Bird(String name) {
        super(name, 2);
        
    }

    @Override
    public void sleep() {
        // TODO Auto-generated method stub
        System.out.println(this.toString()+ " sleep.");
    }

    @Override
    public void eat() {
        // TODO Auto-generated method stub
        System.out.println(this.toString()+ " eat.");
        
    }
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "Bird("+this.getName()+")";
    }

    @Override
    public void takeoff() {
        System.out.println(this.toString()+ " takeoff.");
        
    }

    @Override
    public void fly() {
        System.out.println(this.toString()+ " fly.");
        
    }

    @Override
    public void landing() {
        System.out.println(this.toString()+ " landing.");
        
    }

    @Override
    public void walk() {
        System.out.println(this.toString()+ " walk.");
        
    }

    @Override
    public void run() {
        System.out.println(this.toString()+ " run.");
        
    }
}
