package com.thanakit;

public class Crocodile extends Animal implements Crawlable,Swimable {
    
    public Crocodile(String name) {
        super(name, 4);
        //TODO Auto-generated constructor stub
    }

    @Override
    public void sleep() {
        
        System.out.println(this.toString()+ " sleep.");
    }

    @Override
    public void eat() {
        
        System.out.println(this.toString()+ " eat.");
        
    }
    @Override
    public void swim() {
        System.out.println(this.toString()+ " swim.");
        
    }
    @Override
    public void crawl() {
        System.out.println(this.toString()+ " crawl.");
        
    }
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "Crocodile("+this.getName()+")";
    }
}
