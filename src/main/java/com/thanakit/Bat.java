package com.thanakit;

public class Bat extends Animal implements Flyable,Walkable{

    public Bat(String name) {
        super(name, 2);
        //TODO Auto-generated constructor stub
    }

    @Override
    public void sleep() {
        // TODO Auto-generated method stub
        System.out.println(this.toString()+ " sleep.");
    }

    @Override
    public void eat() {
        // TODO Auto-generated method stub
        System.out.println(this.toString()+ " eat.");
        
    }
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "Bat("+this.getName()+")";
    }

    @Override
    public void takeoff() {
        System.out.println(this.toString()+ " takeoff.");
        
    }

    @Override
    public void fly() {
        System.out.println(this.toString()+ " fly.");
        
    }

    @Override
    public void landing() {
        System.out.println(this.toString()+ " landing.");
        
    }

    @Override
    public void walk() {
        System.out.println(this.toString()+ " walk.");
        
    }

    @Override
    public void run() {
        System.out.println(this.toString()+ " run.");
        
    }
    
}
