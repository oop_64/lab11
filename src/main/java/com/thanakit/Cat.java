package com.thanakit;

public class Cat extends Animal implements Walkable {
    
    public Cat(String name) {
        super(name, 4);
        //TODO Auto-generated constructor stub
    }

    @Override
    public void sleep() {
        
        System.out.println(this.toString()+ " sleep.");
    }

    @Override
    public void eat() {
        
        System.out.println(this.toString()+ " eat.");
        
    }
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "Cat("+this.getName()+")";
    }

    @Override
    public void walk() {
        System.out.println(this.toString()+ " walk.");
        
    }

    @Override
    public void run() {
        System.out.println(this.toString()+ " run.");
        
    }
}
