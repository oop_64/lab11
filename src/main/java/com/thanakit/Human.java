package com.thanakit;

public class Human extends Animal implements Walkable, Swimable{

    public Human(String name) {
        super(name, 2);
        //TODO Auto-generated constructor stub
    }

    @Override
    public void sleep() {
        System.out.println(this.toString()+ " sleep.");
        
    }

    @Override
    public void eat() {
        System.out.println(this.toString()+ " eat.");
        
    }
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "Human("+this.getName()+")";
    }

    @Override
    public void walk() {
        System.out.println(this.toString()+ " walk.");
        
    }

    @Override
    public void run() {
        System.out.println(this.toString()+ " run.");
        
    }

    @Override
    public void swim() {
        System.out.println(this.toString()+ " swim.");
        
    }
    
}
