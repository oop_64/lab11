package com.thanakit;

public class Fish extends Animal implements Swimable {
    
    public Fish(String name) {
        super(name, 0);
        //TODO Auto-generated constructor stub
    }

    @Override
    public void sleep() {
        
        System.out.println(this.toString()+ " sleep.");
    }

    @Override
    public void eat() {
        
        System.out.println(this.toString()+ " eat.");
        
    }
    @Override
    public void swim() {
        System.out.println(this.toString()+ " swim.");
        
    }
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "Fish("+this.getName()+")";
    }
}
